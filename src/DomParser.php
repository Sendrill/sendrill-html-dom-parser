<?php
namespace Sendrill;
require_once __DIR__ . '/simple_html_dom.php';
class HTMLDOMParser
{
	public $dom;
	public $html;

	protected $campaign_id, $customfields;
	protected $subscriber_id, $message_id;
	protected $tracking_tags = [];
	protected $cname = 'http://www.sendrill.com/';
	protected $tracking_url 	= 'http://www.sendrill.com/v2/tracking/clicks';
	protected $tracking_img_src = 'http://www.sendrill.com/v2/tracking/opens';
	protected $unsub_url = 'http://www.sendrill.com/v2/tracking/unsub';
	protected $track_analytics 	= false;
	protected $track_clicks 	= false;
	protected $track_opens 		= true;

	public function __construct($data) {
		
		$this->replace_domain_with_cname($data);
		
		$dom = str_get_html($data['content']);
		$this->dom = $dom;
		
		$this->campaign_id = $data['campaign_id'];
		$this->message_id = $data['message_id'];
		$this->tracking_img_src .= "/{$this->message_id}";
		$this->unsub_url .= "/{$this->message_id}";
		
		$this->subscriber_id = $data['subscriber_id'] ?? null;
		
		
		if (isset($data['track_analytics'])) $this->track_analytics = $data['track_analytics'];
		if (isset($data['track_clicks'])) $this->track_clicks = $data['track_clicks'];
		if (isset($data['customfields'])) $this->customfields = $data['customfields'];
		
		$data['description'] = $data['description'] ?? 'Sendrill';
		if ($this->track_analytics) $this->tracking_tags = [
			'utm_campaign'=>$data['description'],
			'utm_source'=>'newsletter',
			'utm_medium'=>'email',
            'message_id'=> $this->message_id
		];

		return $this->GetHTML();
	}

	public function GetHTML(){
		
		if (isset($this->message_id)):
			if ($this->track_analytics) $this->add_analytics_tracking();
			if ($this->track_clicks) 	$this->add_click_tracking();
			if ($this->track_opens) 	$this->add_tracking_pixel();
		endif;
		
		$this->html = (string) $this->dom;

		$this->remove_tabs_new_lines();
		$this->add_recipient_customfields();
		$this->add_unsubscribe_tags();
		$this->add_global_customfields();
	}

	
	/*	
	* Function for removing tabs/spaces in order for email to be sent minified.
	*/
	private function remove_tabs_new_lines(){
		$this->html = str_replace( [ "\r", "\n", "\t", "    ", "   ", "  "  ], ' ', $this->html);
	}
	/*
	*	Function for user customfields.
	*/
	private function add_recipient_customfields(){
		if (isset($this->customfields) && is_object($this->customfields)){
			foreach ($this->customfields as $key=>$value) {
				$this->html = str_replace( '{'.$key.'}', $value, $this->html );
			}
		}
	}
	private function add_unsubscribe_tags(){
		// create current timestamps for minutes
		$pattern = '/{unsubscribe:(.*)}|{unsubscribe}/i';
		$matches = [];
		if ( preg_match_all($pattern, $this->html, $matches ) > 0){
			for ($i=0; $i<count($matches[0]);$i++){
				$match = $matches[0][$i];
				$r = $matches[1][$i];
				if ($r) $this->unsub_url .= "?url={$matches[1][$i]}";
				$this->html = str_replace( $match, $this->unsub_url, $this->html );
			}
		}
	}
	
	/*
	 * Function adding analytics params on links ;)
	*/
	private function add_analytics_tracking($tracking_tags = false) {
		if (!$tracking_tags) $tracking_tags = $this->tracking_tags;
		//load the html
		foreach ($this->dom->find('a') as $element)  //find link tags atgs-> collection of tags
		{
			$url = $element->href;
			if ($this->isValidUrl($url)) {
				foreach ($tracking_tags as $name => $value) {
					$url = $this->AddDelimiter($url);        //add the delimiter to the url ?/&
					$url = $url . $name . '=' . $value;            //add the analytics parameter
				}
				$element->href = $url;
			}
		}
		return $this->dom;
	}


	/*
	 * Function for adding tracking url on link ;)
	*/
	private function add_click_tracking() {

		foreach ($this->dom->find('a') as $element)  //find link tags atgs-> collection of tags
		{
			if ($this->isValidUrl($element->href)) {
				$element->href = "{$this->tracking_url}/{$this->message_id}?url={$element->href}";
			}
		}
		return $this->dom;
	}
	
	private function add_global_customfields(){
		if (isset($this->subscriber_id)) $this->html = str_replace('{subscriber_id}', "{$this->subscriber_id}", $this->html);
		if (isset($this->campaign_id)) $this->html = str_replace('{campaign_id}', "{$this->campaign_id}", $this->html);
		if (isset($this->message_id)) $this->html = str_replace('{message_id}', "{$this->message_id}", $this->html);
		$this->html = str_replace('{cname}', $data['cname']??'sendrill.com', $this->html);
		$this->html = $this->create_timestamps($this->html);
	}
		
	private function create_timestamps($html){
		$time = time();

		// create current timestamps for hours 
		$pattern = '/{time\+(\d+)hours}/i';
		$matches = [];
		if ( preg_match_all($pattern, $html, $matches ) > 0){
			for ($i=0; $i<count($matches[0]);$i++){
				$match = $matches[0][$i];
				$html = str_replace( $match, $time + (60 * 60 * $matches[1][$i]),$html );
			}
		}
		
		// create current timestamps for days 
		$pattern = '/{time\+(\d+)days}/i';
		$matches = [];
		if ( preg_match_all($pattern, $html, $matches ) > 0){
			for ($i=0; $i<count($matches[0]);$i++){
				$match = $matches[0][$i];
				$html = str_replace( $match, $time + (24 * 60 * 60 * $matches[1][$i]),$html );
			}
		}
		return $html;
	}
	
	

	private function add_tracking_pixel(){
		$body = $this->dom->find('body', 0) ?? $this->dom->find('html', 0);
		if ( isset($body) )
			$body->innertext .= "<img src='{$this->tracking_img_src}' width='1' height='1' alt='' />";
		return $this->dom;
	}

	private function replace_domain_with_cname($data){
		if (isset($data['is_valid_cname']) && isset($data['cname']) && $data['is_valid_cname']):
			$this->cname = str_replace('https://www.sendrill.com/', "http://{$data['cname']}/", $this->cname);
			$this->cname = str_replace('http://www.sendrill.com/', "http://{$data['cname']}/", $this->cname);
			$this->tracking_url = str_replace('https://www.sendrill.com/', "http://{$data['cname']}/", $this->tracking_url);
			$this->tracking_url = str_replace('http://www.sendrill.com/', "http://{$data['cname']}/", $this->tracking_url);
			$this->tracking_img_src = str_replace('https://www.sendrill.com/', "http://{$data['cname']}/", $this->tracking_img_src);
			$this->tracking_img_src = str_replace('http://www.sendrill.com/', "http://{$data['cname']}/", $this->tracking_img_src);
			$this->unsub_url = str_replace('https://www.sendrill.com/', "http://{$data['cname']}/", $this->unsub_url);
			$this->unsub_url = str_replace('http://www.sendrill.com/', "http://{$data['cname']}/", $this->unsub_url);
		endif;
	}
	/*
	 * Add the appropriate delimiter ? for the first and then & for the rest.
	 */
	private function AddDelimiter($url) {

		if (!empty($url)) // only edit the attribute if it's set
		{
			// check if we need to append with ? or &
			if (strpos($url, '?') === false)
				$url .= '?';
			else
				$url .= '&';
		}
		return $url;
	}

	// private function isValidUrl($url) {
		// if (filter_var($url, FILTER_VALIDATE_URL) === false) return false;
		// return true;
	// }
	private function isValidUrl($url){
		if ( strpos($url, 'mailto:') !== false) return false;

		if (strpos($url, $this->cname) !== false) return false;
		
		if (strpos($url, 'sendrill.com') !== false) return false;
		
		if (strpos($url, 'unsubscribe') !== false) return false;
		
		if (strpos($url, 'unsub') !== false) return false;
		
		if (strpos($url, 'tel:') !== false) return false;
		
		return (bool)parse_url($url);		
	}

}