## Sendrill PHP SDK 

[![Latest Stable Version](http://login.sendrill.com/images/icons/logo.png)](https://packagist.org/packages/sendrill/sdk)
[![License](http://opensource.org/files/garland_logo.png)](http://opensource.org/licenses/MIT)

Sendrill SDK is a library that allows you to communicate with Sendrill Platform offering you : 
 
* Sending Transactional Emails on your client's inbox
* Retrieve User Profile
* Update User Profile on Demand.
* Subscribe Users to Sendrill Platform
* Unsubscribe Users

## Official Documentation

Documentation for the Application Programming Interface can be found on the [Sendrill website](http://apidocs.sendrill.com).

## Security Vulnerabilities

If you discover a security vulnerability within Sendrill, please send an e-mail to Giorgos Giakoumettis at hello@sendrill.com. All security vulnerabilities will be promptly addressed.

### License

Sendrill SDK is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## Installation

```json
"require": {
	"sendrill/sdk": "1.1.0",
}	
```

## Example of Usage




```php
$sd = new \Sendrill\Sendrill('Sendrill_APIKEY');

/* Send Sendrill Template */
$result = $sd->Messages->SendTemplate(1,[
  	"SenderEmail"=>"goutis@esolutions.gr",
  	"To"=>"giorgos@yiakoumettis.gr",
	"Subject"=>"Test Subject 15",
   	"Content"=>[
		"Name"=>"Γιώργος",
		"Email"=>"Test",
		"Message"=>"SuperTest"
	]
]);

/* Send Transactional Email */
$result = $sd->Messages->SendEmail([
    "SenderEmail"=>"goutis@esolutions.gr",
    "To"=>"giorgos@yiakoumettis.gr",
    "Subject"=>"Test Subject",
    "Content"=>"Good Evening <br/><br/> Best Regards,<br/> Sendrill.com"
]);
```

